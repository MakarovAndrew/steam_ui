package by.a1qa.utils;

public class ConfigManager {

    public static final String pathToConfig = "./resources/config/startUrlAndQuery.json";
    public static final int SHORT_TIMEOUT = JsonToObject.getValueByKey(pathToConfig, "SHORT_TIMEOUT");
    public static final int MEDIUM_TIMEOUT = JsonToObject.getValueByKey(pathToConfig, "MEDIUM_TIMEOUT");
    public static final int LONG_TIMEOUT = JsonToObject.getValueByKey(pathToConfig, "LONG_TIMEOUT");
    public static final String searchQuery = JsonToObject.getValueByKey(pathToConfig, "searchQuery");
    public static final int countOfComparedGames = JsonToObject.getValueByKey(pathToConfig, "countOfGames");
    public static final String firstSearchPath = "./resources/data/Games.json";
    public static final String secondSearchPath = "./resources/data/Games2.json";
    public static final String pathToLanguages = "./resources/data/languages.json";
}