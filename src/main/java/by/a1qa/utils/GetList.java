package by.a1qa.utils;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;

public class GetList {

    public static List<String> getList (List<WebElement> elements, String regexp) {
        List<String> list = new ArrayList<>();
        int i = 0;
        for (WebElement element : elements) {
            list.add(i, element
                        .getAttribute("href")
                        .replaceFirst("/$", "")
                        .replaceAll(regexp, ""));
            i++;
        }
        return list;
    }
}