package by.a1qa.utils;

import by.a1qa.driver_manager.BrowserFactory;

public class WindowHandle {

    public static void focusToNewTab (String originalHandle) {
        for (String windowHandle : BrowserFactory.getDriver().getWindowHandles()) {
            if(!originalHandle.contentEquals(windowHandle)) {
                BrowserFactory.getDriver().switchTo().window(windowHandle);
                break;
            }
        } 
    } 
}