package by.a1qa.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import by.a1qa.driver_manager.Browser;

public class JsonToObject {

    public static <T> T getValueByKey(String path, String key) {

        try {
            return new HashMap<String,T>(JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<Map<String, T>>(){})).get(key);
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }

    /*public static List<?> readObjectList (String path) {

        try {
            return Arrays.asList(JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<List<?>>(){}));
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }
    
    public static <T> T readObject (String path) {
        try {
            return JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<T>(){});
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }*/
    
    public static List<String> getLanguageListFromJson (String pathToJSON) {

        List<String> langs = new ArrayList<>();

        try {
            File file = new File(pathToJSON);
            for (Language lang : JsonMapper.getMapper().readValue(file, Language[].class)) {
                langs.add(lang.getLanguage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return langs;
    }

    
    public static  List<Game> readGameObject (String pathToJSON) {

        List<Game> list = null;

        try {
            list = Arrays.asList(JsonMapper.getMapper().readValue(Paths.get(pathToJSON).toFile(), Game[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Browser readBrowserObject(String pathToJSON) {

        Browser browser = null;

        try {
            browser = JsonMapper.getMapper().readValue(Paths.get(pathToJSON).toFile(), Browser.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return browser;
    }
}