package by.a1qa.utils;

import java.util.List;

public class Game {
       
        private String name;
        private List<String> platform;
        private String releaseDate;
        private String review;
        private String price;

        public Game(String name, List<String> platform, String releaseDate, String review, String price) {
            this.name = name;
            this.platform = platform;
            this.releaseDate = releaseDate;
            this.review = review;
            this.price = price;
        }

        public Game () {}

        public String getName() {
            return this.name;
        }

        public List<String> getPlatform() {
            return this.platform;
        }

        public String getReleaseDate() {
            return this.releaseDate;
        }

        public String getReview() {
            return this.review;
        }

        public String getPrice() {
            return this.price;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPlatform(List<String> platform) {
            this.platform = platform;
        }

        public void setReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null || getClass() != obj.getClass()) return false;

            Game otherGame = (Game) obj;

            return name.equals(otherGame.name) &&
                platform.equals(otherGame.platform) &&
                releaseDate.equals(otherGame.releaseDate) &&
                review.equals(otherGame.review) &&
                price.equals(otherGame.price);
        }
}