package by.a1qa.utils;

import java.io.IOException;
import java.nio.file.Paths;

public class ObjectToJson {

    public static void prettyWriteObjectToJson(Object object, String pathToJSON) {
        try {
            JsonMapper.getWriter(JsonMapper.getMapper()).writeValue(Paths.get(pathToJSON).toFile(), object);    
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}