package by.a1qa.utils;

import java.time.Duration;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import by.a1qa.driver_manager.BrowserFactory;

public class Waiters {

    public static WebElement waitUntilToBeClickable (WebElement element, int duration) {
        return (new WebDriverWait(BrowserFactory.getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement waitUntilIsPresent (WebElement element, int duration) {
        return (new WebDriverWait(BrowserFactory.getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.visibilityOf(element));
    }        

    public static boolean waitUntilNumberOfWindows (int tabsCount, int duration) {
        return (new WebDriverWait(BrowserFactory.getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.numberOfWindowsToBe(duration));
    }        
}