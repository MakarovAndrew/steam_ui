package by.a1qa.utils;

public class Language {

    private String language;
    
    public Language(String language) {
        this.language = language;
    }

    public Language () {};

    public String getLanguage () {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}