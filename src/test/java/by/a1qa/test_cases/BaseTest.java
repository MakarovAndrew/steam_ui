package by.a1qa.test_cases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.pages.SteamPage;
import by.a1qa.utils.JsonToObject;
import static by.a1qa.utils.ConfigManager.*;

public abstract class BaseTest {

    protected static SteamPage steamPage;
    protected static String originalHandle;

    @BeforeTest
    public void invokeMainPage() {       
        BrowserFactory.getDriver().get(JsonToObject.getValueByKey(pathToConfig, "startUrl"));
        originalHandle = BrowserFactory.getDriver().getWindowHandle();
        steamPage = new SteamPage();
    }

    @AfterTest
    public void killDriver() {
        BrowserFactory.quitDriver();
    }
}