package by.a1qa.test_cases;

import java.time.LocalDate;
import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.pages.PrivacyPolicyPage;
import by.a1qa.utils.JsonToObject;
import by.a1qa.utils.WindowHandle;
import static by.a1qa.utils.ConfigManager.*;

public class PrivatePolicyTest extends BaseTest {
    
    @Test (description = "Privacy policy")
    public void checkPrivacyPolicy() {
        
        steamPage.privacyPolicyLinkClick();
        WindowHandle.focusToNewTab(originalHandle);
        PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage();
        Assert.assertTrue(privacyPolicyPage.isPrivacyPolicyPageOpened(), "This is not Privacy Policy page!");

        Assert.assertTrue(privacyPolicyPage.formatedLanguageItem().containsAll(JsonToObject.getLanguageListFromJson(pathToLanguages)), "Languages do not match!");
        Assert.assertTrue(privacyPolicyPage.getPolicyRevisionDate().contains("" + LocalDate.now().getYear()), "Policy revision do not signed in the current year!");
        BrowserFactory.closeDriver();  
    }
}