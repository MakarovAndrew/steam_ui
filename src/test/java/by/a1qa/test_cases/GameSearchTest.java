package by.a1qa.test_cases;

import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.pages.SearchResultPage;
import by.a1qa.utils.JsonToObject;
import static by.a1qa.utils.ConfigManager.*;

public class GameSearchTest extends BaseTest {
    
    @Test (description = "Game search")
    public void checkGameSearch() {

        BrowserFactory.getDriver().switchTo().window(originalHandle);
        steamPage.enterToSearcBox(searchQuery);
        SearchResultPage searchResultPage = new SearchResultPage();
        Assert.assertTrue(searchResultPage.isSearchResultPageOpened(), "The Search result page did not open!");

        Assert.assertEquals(searchResultPage.returnSearchBox(), searchQuery, "The Search box does not contains search query!");

        searchResultPage.makeGame(firstSearchPath, countOfComparedGames);
        Assert.assertEquals(searchResultPage.getGame(0).getName(), searchQuery, "The First game's name does not equal to search query!");

        searchResultPage.enterToHeaderSearcBox(searchResultPage.getGame(1).getName());
        SearchResultPage secondSearchResultPage = new SearchResultPage();
        Assert.assertEquals(secondSearchResultPage.returnSearchBox(), 
                            JsonToObject.readGameObject(firstSearchPath).get(1).getName(),
                            "The Search box does not contains search query!");
        
        secondSearchResultPage.makeGame(secondSearchPath, secondSearchResultPage.getSizeOfGameList());
        Assert.assertTrue(JsonToObject.readGameObject(secondSearchPath).containsAll(JsonToObject.readGameObject(firstSearchPath)), 
                          "The Second search list does not include all items from the First search list!");
    }
}