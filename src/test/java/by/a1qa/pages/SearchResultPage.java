package by.a1qa.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.utils.Game;
import by.a1qa.utils.ObjectToJson;
import by.a1qa.utils.Waiters;
import static by.a1qa.utils.ConfigManager.*;

public class SearchResultPage {

    private WebElement searchResultAnchor = BrowserFactory.getDriver().findElement(By.xpath("//div[@id='search_result_container']"));
    private WebElement searchBox = BrowserFactory.getDriver().findElement(By.xpath("//input[@id='term']"));
    private WebElement headerSearchBox = BrowserFactory.getDriver().findElement(By.xpath("//input[@id='store_nav_search_term']"));
    private List<WebElement> searchList = BrowserFactory.getDriver().findElements(By.xpath("//a[@data-gpnav='item']"));
    private List<Game> games = new ArrayList<>();
   
    public void enterToHeaderSearcBox(String string) {
        Waiters.waitUntilIsPresent(headerSearchBox, SHORT_TIMEOUT).sendKeys(string);
        headerSearchBox.sendKeys(Keys.ENTER);
    }
    
    public Game getGame (int item) {
        return games.get(item);
    }

    public boolean isSearchResultPageOpened () {
        return Waiters.waitUntilIsPresent(searchResultAnchor, LONG_TIMEOUT).isDisplayed();
    }

    public String returnSearchBox () {
        return Waiters.waitUntilIsPresent(searchBox, MEDIUM_TIMEOUT).getAttribute("value");
    }
    
    private String returnName (WebElement element) {
        return element.findElement(By.xpath(".//span[@class='title']")).getText();       
    }
    
    private List<String> returnPlatforms (WebElement element) {
        List<String> platforms = new ArrayList<>();
        int i = 0;

        do {
            i++;
            platforms.add(element
                            .findElement(By.xpath(".//span[" + i + "][contains(@class,'platform')]"))
                            .getAttribute("class")
                            .replaceFirst("platform_img ", ""));
        } while (element.findElements(By.xpath(".//span[" + (i + 1) + "][contains(@class,'platform')]")).size() > 0);

        return platforms;
    }

    private String returnReleaseDate (WebElement element) {
        return element.findElement(By.xpath(".//div[contains(@class,'released')]")).getText();       
    }

    private String returnReview (WebElement element) {
        try {
            element.findElement(By.xpath(".//span[contains(@class,'search_review')]")).getAttribute("data-tooltip-html");
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return "";
        }
        return element.findElement(By.xpath(".//span[contains(@class,'search_review')]")).getAttribute("data-tooltip-html");       
    }

    private String returnPrice (WebElement element) {
        return element.findElement(By.xpath(".//div[contains(@class,'search_price')]")).getText();
    }


    public void makeGame(String path, int count) {

        for (int i = 0; i < searchList.size(); i++) {
            if (i != count) {
                games.add(i, new Game(
                    returnName(searchList.get(i)), 
                    returnPlatforms(searchList.get(i)), 
                    returnReleaseDate(searchList.get(i)), 
                    returnReview(searchList.get(i)), 
                    returnPrice(searchList.get(i))));
            } else break;
        }

        ObjectToJson.prettyWriteObjectToJson(games, path);      
    }

    public int getSizeOfGameList () {
        return searchList.size();
    }
}