package by.a1qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.utils.Waiters;
import static by.a1qa.utils.ConfigManager.*;

public class SteamPage {

    private WebElement privacyPolicyLink = BrowserFactory.getDriver().findElement(By.xpath("//div/a[contains(@href,'privacy')]"));
    private WebElement searchBox = BrowserFactory.getDriver().findElement(By.xpath("//input[@id='store_nav_search_term']"));

    public void privacyPolicyLinkClick() {
        Waiters.waitUntilToBeClickable(privacyPolicyLink, LONG_TIMEOUT).click();
    }

    public void enterToSearcBox(String string) {
        Waiters.waitUntilIsPresent(searchBox, SHORT_TIMEOUT).sendKeys(string);
        searchBox.sendKeys(Keys.ENTER);
    }
}