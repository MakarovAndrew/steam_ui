package by.a1qa.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import by.a1qa.driver_manager.BrowserFactory;
import by.a1qa.utils.Waiters;
import static by.a1qa.utils.ConfigManager.*;

public class PrivacyPolicyPage {

    private WebElement privacyPolicyAnchor = BrowserFactory.getDriver().findElement(By.xpath("//div[@id='ssa_box']"));
    private List<WebElement> languageItem = BrowserFactory.getDriver().findElements(By.xpath("//div[@id='languages']/a"));
    private WebElement policyRevision = BrowserFactory.getDriver().findElement(By.xpath("//div[@id='newsColumn']/i[3]"));

    public boolean isPrivacyPolicyPageOpened() {
        return Waiters.waitUntilIsPresent(privacyPolicyAnchor, LONG_TIMEOUT).isDisplayed();
    }

    public List<String> formatedLanguageItem() {

        List<String> list = new ArrayList<>();
        int i = 0;
        for (WebElement element : languageItem) {
            list.add(i, element
                        .getAttribute("href")
                        .replaceFirst("/$", "")
                        .replaceAll(".*/", ""));
            i++;
        }
        return list;
    }

    public String getPolicyRevisionDate() {
        return policyRevision.getText();
    }
}